const express = require('express');
const app = express();
//ici on recup tout le dossier pubic (css, fonts, img, js)
app.use(express.static(__dirname + '/public'));
//ici on gère l'affichage des templates front
app.set('views', './views');
app.set('view engine', 'ejs');

//parse les url
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

/* cors autorise nos appel d'url via la requète ajax*/
const cors = require('cors');
let session = require('express-session');
let parseurl = require('parseurl');

//module pour crypter et comparer par un mot de passe
const bcrypt = require('bcrypt');
const saltRounds = 10;

const list = [
	{name: 'riri', classe: "A", note: 13},
	{name: 'fifi', classe: "B", note: 18},
	{name: 'loulou', classe: "C", note: 11}
]

app.use(session({
  secret: 'love kevin',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 3600000 }
}))

app.use(function (req, res, next) {
  if (!req.session.user) {
    req.session.user = null
    req.session.isLogged = false
  }

//   // get the url pathname   pathname est la section de chemin de l'URL, qui vient après l'hôte et avant la requête
  let pathname = parseurl(req).pathname

//gestion des routes protégées
  let protectedPath = ['/list', /*"/listbdd",*/ "/infos"];

 if(protectedPath.indexOf(pathname) !== -1 && req.session.isLogged === false) {
      res.redirect('/login');
 } else {
      next()
 }

})


let mysql = require('mysql');
let pool  = mysql.createPool({
  connectionLimit : 10000,
    host: "localhost",
    user: "root",
    password: "root",
    database: "session",
	port: 8889 //ou 3306 pour windows
});
     

app.get('/', (req, res, next)=>{
	//on a 3 types de req (récupération de données envoyées par l'utilisateur)
	//req.body    récupère toutes les données envoyées dans le corps du site (formulaire)
	//req.params    récupère toutes les données envoyées dans l'url (id)
	//req.headers    récupère les infos envoyées dans l'entête de la requète ajax
	console.log("welcome")
	//res.send     renvoi notre réponse dans le terminal
	res.render('layout', {template: "home", title: "Page d'accueil", session: req.session}); //renvoi la réponse vers notre template express
	//res.json({maliste: list})
})

app.get('/infos', (req, res, next)=>{
	res.render('layout', {template: "info", firstname: "Antoine", lastname: "Lord", phone: "18", session: req.session});
})

app.get('/list', (req, res, next)=>{
	console.log(req.body)
	res.render('layout', {template: "list", list: list, session: req.session});
})

app.post('/list', (req, res, next)=>{
	console.log(req.body);
	let data = {name: req.body.name, classe: req.body.classe, note: req.body.note}
	list.push(data);
	console.log(list)
	res.redirect('/list');
})

app.get('/listbdd', (req, res, next)=>{
	//console.log(req.body)
	pool.query('SELECT * FROM users', function (error, results, fields) {
        
        console.log(results);
  
		res.render('layout', {template: "listbdd", list: results, session: req.session});
     });
	
})
//une route get pour la page register
app.get('/register', (req, res, next) =>{
	res.render('layout', {template: "register", name: "S'enregistrer", session: req.session});
})

//une route post register pour insérer un nouvel utilisateur (utiliser bcrypt)
app.post('/register', (req, res, next) => {
	//pour bcrypt on va utiliser la fonction hash bcrypt.hash(req.body.password, saltRounds)
	bcrypt.hash(req.body.password, saltRounds)
  .then((hash)=> {
  
    let sql = 'INSERT INTO users(email, psw, firstName, lastName, age) VALUES (?, ?, ?, ?, ?)';

    pool.query(sql, [req.body.email, hash, req.body.firstname, req.body.lastname, req.body.age], (err, result, fields)=>{
      if(err) {
        console.log(err);
      }

      res.redirect('/login');
    })

  })
})


//route d'affichage de la page login
app.get('/infos', (req, res, next)=>{
	res.render('layout', {template: "login", name: "login",error: null, session: req.session});
})


//route d'affichage de la page login
app.get('/login', (req, res, next)=>{
  res.render('layout', {template: "login", name: "Se connecter",error: null, session: req.session});
})

//faire une requète de récupération d'un compte en fonction de l'email entrée
app.post('/login', (req, res, next)=>{
	let sql = 'SELECT * FROM users WHERE email = ?';

    pool.query(sql, [req.body.email], (err, user, fields)=>{
      if(err) {
        console.log(err);
      }
      console.log('USER',user.length)
      //si le mail n'existe pas dans la bdd

		//envoi une erreur en res
      if(user.length === 0) {
        res.render('layout', {template: 'login', error: "Mauvais mail", session: req.session})
      } else {
        console.log(user[0].firstName)
      	//on vérifie si les password match avec bcrypt.compare(password entré par l'user et le password récup dans la bdd) renvoi true ou false
         bcrypt.compare(req.body.password, user[0].psw)
        .then((same)=>{
          console.log('same', same);
          //si c'est true
          if(same) {
          	//création de la session
            req.session.user = {
              firstName: user[0].firstName,
              lastName: user[0].lastName,
              email: user[0].email
            }

            req.session.isLogged = true
            res.redirect('/');
          } else {
          	//res.render avec l'erreur'
            res.render('layout', {template: 'login', error: "Le mot de passe est erroné ! ", session: req.session})
          }
        })
      }      
    })
})
	


app.get('/logout', (req, res, next)=> {
 req.session.destroy((err) =>{
   // cannot access session here
   res.redirect('/');
  })

})


const PORT = process.env.PORT || 5000;
app.listen(PORT, ()=>{
	console.log('listening port '+PORT+' all is ok');
})
